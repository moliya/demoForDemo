//
//  MOViewControllerSwizzling.m
//  demoForDemo
//
//  Created by moliya on 15/5/26.
//  Copyright (c) 2015年 Wanquan Network Technology. All rights reserved.
//

#import "MOViewControllerSwizzling.h"
#import "Aspects.h"
#import <UIKit/UIKit.h>

@implementation MOViewControllerSwizzling

+ (void)load {
    //load方法会在应用启动的时候自动被runtime调用，通过重载该方法实现最小的对业务方的代码入侵
    [super load];
    [MOViewControllerSwizzling sharedInstance];
}

/**
 *  @author carefree
 *
 *  @brief  生成单例
 *
 *  @return 返回自身的单例
 */
+ (instancetype)sharedInstance {
    static dispatch_once_t onceToken;
    static MOViewControllerSwizzling *sharedInstance;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[MOViewControllerSwizzling alloc] init];
    });
    return sharedInstance;
}

- (instancetype)init{
    self = [super init];
    if (self) {
        /**
         *在这里做方法拦截
         */
        //拦截loadView，在loadView执行之后再执行自定义的方法
        [UIViewController aspect_hookSelector:@selector(loadView) withOptions:AspectPositionAfter usingBlock:^(id<AspectInfo>aspectInfo) {
            [self loadView:[aspectInfo instance]];
        }error:NULL];
        //拦截viewWillAppear，在viewWillAppear执行之前先执行自定义的方法
        [UIViewController aspect_hookSelector:@selector(viewWillAppear:) withOptions:AspectPositionBefore usingBlock:^(id<AspectInfo>aspectInfo, BOOL animated) {
            [self viewWillAppear:animated viewController:[aspectInfo instance]];
        }error:NULL];
    }
    return self;
}

/**
 *拦截到方法后需要完成的操作
 */
//loadView
- (void)loadView:(UIViewController *)viewController {
    /* 你可以使用这个方法进行打日志，初始化基础业务相关的内容 */
    NSLog(@"[%@ loadView]", [viewController class]);

}

//viewWillAppear
- (void)viewWillAppear:(BOOL)animated viewController:(UIViewController *)viewController {
    /* 你可以使用这个方法进行打日志，初始化基础业务相关的内容 */
    NSLog(@"[%@ viewWillAppear:%@]", [viewController class], animated ? @"YES" : @"NO");
}


@end
